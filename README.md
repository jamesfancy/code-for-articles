[给 .NET 程序加个「设置开机启动」 - 边城客栈 - SegmentFault 思否](https://segmentfault.com/a/1190000043473615)



# 示例代码库分支目录

> 更新的在前

## [2023-02-17] 解决 Windows 版微信 3.9 收到文件“只读”的问题

### 代码分支

- [WxFilesWritable](https://gitee.com/jamesfancy/code-for-articles/tree/WxFilesWritable/)

### 相关文章

- SegmentFault 思否

    - [写个 .NET 程序解决 Windows 版微信 3.9 收到文件“只读”的问题 - 边城客栈 ](https://segmentfault.com/a/1190000043437787)
    - [给 .NET 程序加个「设置开机启动」 - 边城客栈](https://segmentfault.com/a/1190000043473615)

- 51CTO 博客

    - [写个 .NET 程序解决 Windows 版微信 3.9 收到文件“只读”的问题 - 边城客栈](https://blog.51cto.com/jamesfancy/6064252)
    - [给 .NET 程序加个「设置开机启动」_边城客栈](https://blog.51cto.com/jamesfancy/6086963)


## [2021-04-15] Web 前后端安全传输

### 代码分支

- [secure-transmiting](https://gitee.com/jamesfancy/code-for-articles/tree/secure-transmiting/)，传输演示
- [secure-intermediator-demo](https://gitee.com/jamesfancy/code-for-articles/tree/secure-intermediator-demo/)，中间人攻击演示

### 相关文章

- [安全地在前后端之间传输数据 - 「1」技术预研 - 边城客栈 - SegmentFault 思否](https://segmentfault.com/a/1190000039827138)
- [安全地在前后端之间传输数据 - 「2」注册和登录示例 - 边城客栈 - SegmentFault 思否](https://segmentfault.com/a/1190000039844746)
- [安全地在前后端之间传输数据 - 「3」真的安全吗？ - 边城客栈 - SegmentFault 思否](https://segmentfault.com/a/1190000039895855)